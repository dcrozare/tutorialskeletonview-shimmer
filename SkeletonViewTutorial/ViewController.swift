//
//  ViewController.swift
//  SkeletonViewTutorial
//
//  Created by Diego Crozare on 21/05/19.
//  Copyright © 2019 Diego Crozare. All rights reserved.
//

import UIKit
import SkeletonView

class ViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [image, label1, label2, label3].forEach {
            $0?.showAnimatedGradientSkeleton()
        }
        
    }

}

